#!/bin/bash
set -euo pipefail

make run-cluster
ssh root@worker 'killall -9 sshd' || true
