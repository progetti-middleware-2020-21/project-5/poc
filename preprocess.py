#!/usr/bin/env python3
""" Preprocess owid data file to render a tabulated csv with daily
    new cases, with countries as headers and days as rows
"""

import argparse
import csv
import random


def intc(val):
    """ Safe str -> int conversion """
    try:
        return int(float(val))
    except ValueError:
        return None


def zero_dict(keys, vdict):
    """ Given a keys and dict return dict with missing keys set to 0 """
    ret = {}
    for key in keys:
        if key in vdict and vdict[key]:
            ret[key] = vdict[key]
        else:
            ret[key] = 0
    return ret


def main():
    """ Main method """
    parser = argparse.ArgumentParser(description='Preprocess OWID data')
    parser.add_argument('--no-owid', default=False,
                        action=argparse.BooleanOptionalAction,
                        help="Omit OWID cumulative data")
    parser.add_argument('--pick', '-p', type=float,
                        help="Pick random percent of available countries [0-100]")
    args = parser.parse_args()
    with open('owid-covid-data.csv', newline='') as data_file:
        lines = list(csv.DictReader(data_file, delimiter=","))
    res = {}
    codes, codes_owid = set(), set()
    dates = set()
    for line in lines:
        if line['date'] not in res:
            res[line['date']] = {}
        res[line['date']][line['iso_code']] = intc(line['new_cases'])
        dates.add(line['date'])
        if line['iso_code'].startswith('OWID'):
            codes_owid.add(line['iso_code'])
        else:
            codes.add(line['iso_code'])
    dates = sorted(dates)
    keys = sorted(codes) + (sorted(codes_owid) if not args.no_owid else [])
    if args.pick is not None:
        keys_len = round(min(args.pick / 100, 1.0) * len(keys))
        keys = sorted(random.sample(keys, keys_len))
    with open('data-processed.csv', 'w', newline='') as data_file:
        dictw = csv.DictWriter(data_file, ['date', ] + keys)
        dictw.writeheader()
        for date in sorted(res.keys()):
            dictw.writerow({'date': date, **zero_dict(keys, res[date])})


if __name__ == "__main__":
    main()
