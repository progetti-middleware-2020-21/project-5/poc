#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>

#include <boost/tokenizer.hpp>
#include <boost/mpi.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/algorithm/string/join.hpp>

namespace mpi = boost::mpi;

typedef boost::tokenizer<boost::escaped_list_separator<char>> Tokenizer;

typedef struct CountryIncrease {
    std::string countryCode;
    double percIncrease;
} CountryIncrease;


bool compareCI(CountryIncrease ci1, CountryIncrease ci2) {
    bool ci1inv = std::isnan(ci1.percIncrease) || std::isinf(ci1.percIncrease);
    bool ci2inv = std::isnan(ci2.percIncrease) || std::isinf(ci2.percIncrease);
    if(ci1inv) {
        return false;
    } else if(ci2inv) {
        return true;
    } else {
        return ci1.percIncrease > ci2.percIncrease;
    }
}


int main() {
    mpi::environment env;
    mpi::communicator world;

    std::vector<std::string> header, dates;
    std::vector<int> data;
    int colSize = 0;
    int rowSize = 0;
    int avgSplitSize, avgTotSize,
        avgChangeSplitSize, avgChangeTotSize;
    int limit;

    if(world.rank() == 0) {
        std::ifstream in("data-processed.csv");
        if (!in.is_open()) return 1;

        std::string line;

        getline(in, line);
        Tokenizer tok(line);
        header.assign(++tok.begin(), tok.end());
        header[header.size() - 1].pop_back();

        while (getline(in,line)) {
            rowSize++;
            Tokenizer tok(line);
            std::vector<int> newline;
            dates.push_back(*tok.begin());
            std::transform(++tok.begin(), tok.end(), std::back_inserter(newline),
                           [](std::string s){ return std::stoi(s); });
            if(colSize == 0) {
                colSize = newline.size();
            }
            std::copy(newline.begin(), newline.end(), std::back_inserter(data));
        }
    }
    world.barrier();
    broadcast(world, data, 0);
    broadcast(world, header, 0);
    broadcast(world, colSize, 0);
    broadcast(world, rowSize, 0);

    avgTotSize = (rowSize - 6) * colSize;
    avgSplitSize = avgTotSize / world.size();
    avgChangeTotSize = (rowSize - 7) * colSize;
    avgChangeSplitSize = avgChangeTotSize / world.size();

    std::vector<double> averages;

    limit = world.rank() != world.size() - 1 ?
                avgSplitSize * (world.rank() + 1) : avgTotSize;

    for(int i = avgSplitSize * world.rank();
            i < limit;
            i++) {
        int sum = 0;
        for(int j = 0; j < 7; j++) {
            sum += data[i + colSize * j];
        }
        averages.push_back(double(sum)/7.0);
    }

    std::vector<double> avg;
    if(world.rank() == 0) {
        std::copy(averages.begin(), averages.end(), std::back_inserter(avg));
        for(int i = 1; i < world.size(); i++) {
            std::vector<double> recvAvg;
            world.recv(i, 0, recvAvg);
            std::copy(recvAvg.begin(), recvAvg.end(), std::back_inserter(avg));
        }
        std::ofstream out("data-7-averages.csv");
        for(int i = 0; i < header.size(); i++) {
            out << "," << header[i];
        }
        for(int i = 0; i < avg.size(); i++) {
            if(i % colSize == 0) {
                out << std::endl << dates[(i / colSize) + 6];
            }
            out << "," << avg[i];
        }
        out.close();
    } else {
        world.send(0, 0, averages);
    }
    broadcast(world, avg, 0);

    std::vector<double> averageChanges;
    limit = world.rank() != world.size() - 1 ?
                    avgChangeSplitSize * (world.rank() + 1) : avgChangeTotSize;

    for(int i = avgChangeSplitSize * world.rank();
            i < limit;
            i++) {
        double change = (avg[i + colSize] - avg[i])/avg[i];
        averageChanges.push_back(change);
    }

    std::vector<double> avgChanges;
    if(world.rank() == 0) {
        std::copy(averageChanges.begin(), averageChanges.end(),
                  std::back_inserter(avgChanges));
        for(int i = 1; i < world.size(); i++) {
            std::vector<double> recvAvgCh;
            world.recv(i, 0, recvAvgCh);
            std::copy(recvAvgCh.begin(), recvAvgCh.end(),
                      std::back_inserter(avgChanges));
        }
        std::ofstream out("data-perc-increase.csv");
        for(int i = 0; i < header.size(); i++) {
            out << "," << header[i];
        }
        for(int i = 0; i < avgChanges.size(); i++) {
            if(i % colSize == 0) {
                out << std::endl << dates[(i / colSize) + 7];
            }
            out << ",";
            if(!(std::isnan(avgChanges[i]) || std::isinf(avgChanges[i]))) {
                out << avgChanges[i];
            }
        }
        out.close();
    } else {
        world.send(0, 0, averageChanges);
    }
    broadcast(world, avgChanges, 0);

    int topRows = rowSize - 7;
    std::vector<std::string> tops;
    for(int i = world.rank(); i < topRows; i += world.size()) {
        std::vector<CountryIncrease> dailyIncreases;
        for(int j = 0; j < colSize; j++) {
            CountryIncrease inc;
            inc.countryCode = header[j];
            inc.percIncrease = avgChanges[i * colSize + j];
            dailyIncreases.push_back(inc);
        }
        std::sort(dailyIncreases.begin(), dailyIncreases.end(), compareCI);
        std::stringstream ss;
        for(int j = 0; j < 10; j++) {
            ss << "," << dailyIncreases[j].countryCode;
        }
        tops.push_back(ss.str());
    }



    if(world.rank() == 0) {
        std::vector<std::vector<std::string>> vectors;
        vectors.push_back(tops);
        for(int i = 1; i < world.size(); i++) {
            std::vector<std::string> recvTops;
            world.recv(i, 0, recvTops);
            vectors.push_back(recvTops);
        }
        std::ofstream out("data-top.csv");
        for(int i = 0; i < rowSize - 7; i++) {
            out << dates[i + 7];
            out << vectors[i % world.size()][i / world.size()] << std::endl;
        }
        out.close();
    } else {
        world.send(0, 0, tops);
    }

    return 0;
}
