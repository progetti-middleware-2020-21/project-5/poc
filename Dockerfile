FROM docker.io/library/debian:bullseye

ARG TZ=Europe/Rome
ARG DEBIAN_FRONTEND=noninteractive
ENV OMPI_MCA_btl_vader_single_copy_mechanism=none

RUN apt-get update -yq \
    && apt-get install \
        --no-install-recommends -yq \
        ca-certificates \
        psmisc \
        make \
        g++ \
        libopenmpi-dev \
        libboost-dev \
        libboost-serialization-dev \
        libboost-mpi-dev \
        wget \
        python3 \
        python-is-python3 \
        mpi \
        openssh-server \
        openssh-client \
    && rm -rf /var/lib/apt/lists/*

RUN echo 'PermitEmptyPasswords yes' >> /etc/ssh/sshd_config && \
    echo 'PermitRootLogin yes' >> /etc/ssh/sshd_config && \
    mkdir /run/sshd && \
    echo 'Host *\n  UserKnownHostsFile /dev/null\n  StrictHostKeyChecking no' > /etc/ssh/ssh_config

RUN useradd -m -u 1000 mpi && \
    passwd -d mpi && passwd -d root

USER mpi
