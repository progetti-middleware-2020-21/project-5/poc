# Project 5 (Analysis of COVID-19 Data)

## Repository structure

The repository contains two components:
* a preprocessor written in python, `preprocessor.py`
* the real data analyzer, written in C++ `mpi.cpp`


The proposed solution can be build and run with `make run`,
which will call the preprocessor script and run the mpi computation. 

## Implementation
The Makefile has 4 targets (+ clean target) which self-explain the high-level structure
* `compile`: compile the MPI processing program
* `preprocess`: before running the MPI computation, download the CSV file and run `preprocessor.py`
* `run`: build and execute the MPI project in multi-process mode, which depends on (depends on `compile` and `preprocess`)
* `run-cluster`: target for the docker-compose env in order the project in cluster mode (depends on `compile` and `preprocess`)

### Preprocessing script
The `preprocessor.py` will get as input the raw CSV file and convert it in a CSV style matrix where
* column represent the nation reporting new cases
* the row the date of a report

### MPI processing
The data processing part is implemented in C++ using Boost.MPI library.

The implemented algorithm can be summarised in the following steps
1. First load with the process 0 the processed CSV and broadcast the data to the other processes.
2. Compute a fair data chunks division for every process
3. For 7-day-average, Percentage increase and Top 10 country with the highest percentage
   1. Compute the requested data by splitting the job across the process, using the data chunks computed before
   2. Then:
      1. If rank == 0, receive all the computed data and export the CSV
      1. Else send to rank 0 the computed data
   3. Broadcast the data union from 0 to the other processes


## Dependencies
in order to build the source you need the following dependencies:

* compile the source that compute the analysis (`mpi.cpp`)
  > g++
    libopenmpi-dev
    libboost-dev
    libboost-serialization-dev
    libboost-mpi-dev
* for covid dataset download and pre-processing:
  > wget
    python
* for the cluster execution (runned over ssh)
  > openssh-server
    openssh-client
* run the build (which exec `mpirun`)
  > mpi
  
## Start the project

### On bare metal (multi-process)
1. install al the required dependencies
2. Execute `make run`

### Run in docker-compose (cluster)
The repository include also a Dockerfile which generates an image with all the dependencies for the project.

You can run the program in this Dockerfile with the docker-compose service "poc5", which will mount the code in the working direcotry
```sh
docker-compose up
```
