MPICPP ?= mpic++
LD_FLAGS=-lboost_mpi -lboost_serialization

compile:
	$(MPICPP) -o mpi mpi.cpp $(LD_FLAGS)

preprocess:
	@wget https://covid.ourworldindata.org/data/owid-covid-data.csv \
		 -O owid-covid-data.csv
	@python preprocess.py

run: compile preprocess
	mpirun -np 4 ./mpi

run-cluster: compile preprocess
	mpirun -np 4 -H worker:2,127.0.0.1:2 /app/mpi

clean:
	@rm -f data-perc-increase.csv data-processed.csv data-top.csv \
		   data-7-averages.csv owid-covid-data.csv mpi
